describe Currency do
  it { is_expected.to have_many(:values) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_inclusion_of(:symbol).in_array(CurrencySymbol.all) }
end
