describe CurrencyValue do
  it { is_expected.to belong_to(:currency) }

  it { is_expected.to validate_presence_of(:currency) }
  it { is_expected.to validate_presence_of(:date) }
  it { is_expected.to validate_presence_of(:value) }
end
