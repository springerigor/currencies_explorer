# README

The app is responsible for displaying values of three (pseudo)currencies:

* Bitcoin
* Etherium
* NASDAQ (Microsoft stock price)

## How to check production app?

Just visit [https://frozen-caverns-58970.herokuapp.com/currencies](https://frozen-caverns-58970.herokuapp.com/currencies)

## How to setup the app locally?

The app requires PostgreSQL, Ruby 2.4.1 and Rails 5.1.1. To install all required gems just run:

```bash
bundle install
```

Configure your database by adjusting `config/database.yml` file

Setup the schema and seeds for development and test databases:

```bash
rake db:setup
rake db:create RAILS_ENV=test
```

Configure your dotenv's ENV variables:
```bash
cp .env.example .env
```

## Running local application server

If you want to run a local sever all you need to do is run:

```bash
rails s -p 7000
```

`-p` is optional and it specifies a port which the server will bind to.

## How to run tests?

The app uses `rspec` gem for testing purposes. To run the whole test suit please execute:

```bash
rspec
```

## How to import fresh data?

There are three rake tasks responsible for fetching up-to-date data in an idempotent way.

1. To import Bitcoin values: `rake imports:bitcoin`
2. To import Etherium values: `rake imports:etherium`
3. To import NASDAQ values: `rake imports:nasdaq`

## TODO:

- [ ] Cover the gateways with unit tests (with mocked API responses)
- [ ] Cover the jobs with unit tests
- [ ] Add `sidekiq` and a scheduler to schedule daily execution of the import jobs to keep the chart up-to date
- [ ] Write a feature spec to check if the chart is rendered correctly (`capybara`)
- [ ] Extract JS code from the view
- [ ] Dockerize the app
