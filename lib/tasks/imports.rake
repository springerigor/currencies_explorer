namespace :imports do
  desc "Import Bitcoin values"
  task bitcoin: :environment do
    ImportBitcoinValuesJob.perform_now
  end

  desc "Import Etherium values"
  task etherium: :environment do
    ImportEtheriumValuesJob.perform_now
  end

  desc "Import NASDAQ values"
  task etherium: :environment do
    ImportNasdaqValuesJob.perform_now
  end
end
