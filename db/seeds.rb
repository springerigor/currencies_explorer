[
  { name: 'Bitcoin', symbol: CurrencySymbol::BITCOIN },
  { name: 'Etherium', symbol: CurrencySymbol::ETHERIUM },
  { name: 'Nasdaq Stock Market', symbol: CurrencySymbol::NASDAQ },
].each do |currency|
  Currency.create!(
    name: currency[:name],
    symbol: currency[:symbol],
  )
end
