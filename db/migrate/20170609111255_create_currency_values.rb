class CreateCurrencyValues < ActiveRecord::Migration[5.1]
  def change
    create_table :currency_values do |t|
      t.decimal :value, precision: 8, scale: 2, null: false
      t.datetime :date, null: false, index: true

      t.references :currency, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end
