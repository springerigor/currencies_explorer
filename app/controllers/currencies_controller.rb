class CurrenciesController < ApplicationController
  def index
    @bitcoin_values = currency_values(CurrencySymbol::BITCOIN)
    @etherium_values = currency_values(CurrencySymbol::ETHERIUM)
    @nasdaq_values = currency_values(CurrencySymbol::NASDAQ)
  end

  private

  def currency_values(currency)
    CurrencyValue.public_send(currency.downcase).pluck(:date, :value).map do |date, value|
      [date.to_f * 1000, value.to_f]
    end
  end
end
