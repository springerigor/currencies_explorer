class EtherchainGateway < HttpGateway
  base_uri 'https://etherchain.org/api'

  def etherium_values
    JSON.parse(self.class.get('/statistics/price').body)['data']
  end
end
