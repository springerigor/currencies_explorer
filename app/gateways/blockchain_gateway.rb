class BlockchainGateway < HttpGateway
  base_uri 'https://blockchain.info/'

  def bitcoin_values
    JSON.parse(self.class.get('/charts/market-price?timespan=365days&format=json').body)['values']
  end
end
