class AlphaVantageGateway < HttpGateway
  base_uri 'http://www.alphavantage.co/'

  def nasdaq_values
    JSON.parse(self.class.get("/query?function=TIME_SERIES_DAILY&symbol=MSFT&outputsize=compact&apikey=#{ ENV['ALPHA_VANTAGE_API_KEY'] }").body)['Time Series (Daily)']
  end
end
