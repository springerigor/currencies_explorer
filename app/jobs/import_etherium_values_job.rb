class ImportEtheriumValuesJob < ActiveJob::Base
  def perform
    ehterium_values.each do |record|
      CurrencyValue.find_or_create_by!(
        currency: etherium,
        value: record['usd'],
        date: record['time'].to_date,
      )
    end
  end

  private

  def ehterium_values
    EtherchainGateway.new.etherium_values
  end

  def etherium
    @ehterium ||= Currency.find_by!(symbol: CurrencySymbol::ETHERIUM)
  end
end
