class ImportBitcoinValuesJob < ActiveJob::Base
  def perform
    bitcoin_values.each do |record|
      CurrencyValue.find_or_create_by!(
        currency: bitcoin,
        value: record['y'],
        date: Time.at(record['x']).to_date,
      )
    end
  end

  private

  def bitcoin_values
    BlockchainGateway.new.bitcoin_values
  end

  def bitcoin
    @bitcoin ||= Currency.find_by!(symbol: CurrencySymbol::BITCOIN)
  end
end
