class ImportNasdaqValuesJob < ActiveJob::Base
  def perform
    nasdaq_values.each do |date, details|
      CurrencyValue.find_or_create_by!(
        currency: nasdaq,
        value: details['4. close'],
        date: date.to_date,
      )
    end
  end

  private

  def nasdaq_values
    AlphaVantageGateway.new.nasdaq_values
  end

  def nasdaq
    @ehterium ||= Currency.find_by!(symbol: CurrencySymbol::NASDAQ)
  end
end
