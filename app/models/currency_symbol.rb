module CurrencySymbol
  BITCOIN = 'BTC'
  ETHERIUM = 'ETH'
  NASDAQ = 'NASDAQ'

  def self.all
    [BITCOIN, ETHERIUM, NASDAQ]
  end
end
