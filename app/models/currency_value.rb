class CurrencyValue < ApplicationRecord
  belongs_to :currency

  validates :currency, presence: true
  validates :date, presence: true
  validates :value, presence: true

  scope :btc, -> { where(currency: Currency.find_by(symbol: CurrencySymbol::BITCOIN)) }
  scope :eth, -> { where(currency: Currency.find_by(symbol: CurrencySymbol::ETHERIUM)) }
  scope :nasdaq, -> { where(currency: Currency.find_by(symbol: CurrencySymbol::NASDAQ)) }
end
