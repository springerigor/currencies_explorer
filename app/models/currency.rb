class Currency < ApplicationRecord
  has_many :values, class_name: 'CurrencyValue'

  validates :name, presence: true
  validates :symbol, presence: true, inclusion: { in: CurrencySymbol.all }

end
